# Trabalho 1 de PPD
O trabalho consiste em desenvolver o jogo Trilha, utilizando sockets para a comunicação.
Além disso o jogo conta com um chat.

### Instalação 
Clone o repositório 
```http
git clone https://gitlab.com/Hozana-Izadora/t1_ppd.git  
```
Instale a lib necessária, neste caso é o PySide2

```http
pip install PySide2
```
